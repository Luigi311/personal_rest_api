# Set to python3
#!/usr/bin/env python3

from flask import Flask, render_template
import os, traceback, argparse
from scripts.functions import notify

# Functions for pages
from scripts.functions import staking_all
from scripts.lido.lido import staking_eth, staking_sol

from scripts.terra.staking import staking_luna
from scripts.terra.terraswap import (
    farming_terraswap,
    protocol_terraswap,
    volume_terraswap,
)
from scripts.terra.terra import protocol_terra_output
from scripts.terra.anchor import farming_anchor, protocol_anchor
from scripts.terra.apollo import protocol_apollo_output

from scripts.secret.secretswap import farming_secretswap, protocol_secretswap
from scripts.secret.siennaswap import farming_siennaswap, protocol_siennaswap

from scripts.avalanche.sushiswap import get_avalanche_sushiswap

from scripts.ethereum.uniswap import get_ethereum_uniswap

from scripts.coin.sifuvision import get_sifuvision

parser = argparse.ArgumentParser(description="Rest API developed by luigi311")
parser.add_argument("--port", type=int, default=8080, help="Port to run the server on")
parser.add_argument(
    "--host", type=str, default="0.0.0.0", help="Host to run the server on"
)
parser.add_argument("--debug", action="store_true", help="Debug mode")
args = parser.parse_args()

api = Flask(__name__)


@api.route("/")
def homepage():
    return render_template("index.html")


@api.route("/staking")
def staking():
    return render_template("staking.html")


@api.route("/staking/all", methods=["GET"])
def get_staking_all():
    output = staking_all()

    return output


@api.route("/staking/luna", methods=["GET"])
def get_staking_luna():
    luna_apr = staking_luna()

    output = {"luna_apr": luna_apr}

    return output


@api.route("/staking/eth", methods=["GET"])
def get_staking_eth():
    eth_apr = staking_eth()

    output = {
        "eth_apr": eth_apr,
    }

    return output


@api.route("/staking/sol", methods=["GET"])
def get_staking_sol():
    sol_apr = staking_sol()

    output = {"sol_apr": sol_apr}

    return output


@api.route("/farming")
def farming():
    return render_template("farming.html")


@api.route("/farming/terra")
def farming_terra():
    return render_template("farming_terra.html")


@api.route("/farming/terra/terraswap", methods=["GET"])
def get_farming_terraswap():
    output = farming_terraswap()

    return output


@api.route("/farming/terra/anchor", methods=["GET"])
def get_farming_anchor():
    output = farming_anchor()

    return output


@api.route("/farming/secret")
def farming_secret():
    return render_template("farming_secret.html")


@api.route("/farming/secret/secretswap", methods=["GET"])
def get_farming_secretswap():
    output = farming_secretswap()

    return output


@api.route("/farming/secret/siennaswap", methods=["GET"])
def get_farming_siennaswap():
    output = farming_siennaswap()

    return output


@api.route("/protocol")
def protocol():
    return render_template("protocol.html")


@api.route("/protocol/terra")
def protocol_terra():
    return render_template("protocol_terra.html")


@api.route("/protocol/terra/terra")
def get_protocol_terra():
    output = protocol_terra_output()

    return output


@api.route("/protocol/terra/terraswap")
def get_protocol_terraswap():
    output = protocol_terraswap()

    return output


@api.route("/protocol/terra/anchor")
def get_protocol_anchor():
    output = protocol_anchor()

    return output


@api.route("/protocol/terra/apollo")
def get_protocol_apollo():
    output = protocol_apollo_output()

    return output


@api.route("/protocol/secret")
def protocol_secret():
    return render_template("protocol_secret.html")


@api.route("/protocol/secret/secretswap")
def get_protocol_secretswap():
    output = protocol_secretswap()

    return output


@api.route("/protocol/secret/siennaswap")
def get_protocol_siennaswap():
    output = protocol_siennaswap()

    return output


@api.route("/volume")
def volume():
    return render_template("volume.html")


@api.route("/volume/terra")
def volume_terra():
    return render_template("volume_terra.html")


@api.route("/volume/terra/terraswap")
def get_volume_terraswap():
    output = volume_terraswap()

    return output


@api.route("/network")
def network():
    return render_template("network.html")

@api.route("/network/avalanche")
def network_avalanche():
    return render_template("network_avalanche.html")

@api.route("/network/avalanche/sushiswap")
def get_network_avalanche_sushiswap():
    output = get_avalanche_sushiswap()

    return output

@api.route("/network/ethereum")
def network_ethereum():
    return render_template("network_ethereum.html")

@api.route("/network/ethereum/uniswap")
def get_network_ethereum_uniswap():
    output = get_ethereum_uniswap()

    return output

@api.route("/coin/sifuvision")
def get_coin_sifuvision():
    output = get_sifuvision()

    return output

if __name__ == "__main__":
    try:
        # if debug mode is enabled
        if args.debug:
            api.run(host=args.host, port=args.port, debug=True)
        else:
            from waitress import serve

            serve(api, host=args.host, port=args.port)

    except Exception as error:
        notify("ERROR!")

        if isinstance(error, list):
            for message in error:
                notify(message)
        else:
            notify(f"{error}")

        notify("Exiting")

        print(traceback.format_exc())

        os._exit(1)

    except KeyboardInterrupt:
        notify("Exiting")
        os._exit(1)
