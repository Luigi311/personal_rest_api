FROM python:slim
ENV PYTHONUNBUFFERED 1
ENV PATH /home/appuser/.local/bin:$PATH

RUN adduser --disabled-password --gecos "" appuser
RUN mkdir /app && chown appuser:appuser /app

USER appuser
WORKDIR /app

# We copy just the requirements.txt first to leverage Docker cache
COPY --chown=appuser:appuser ./requirements.txt /app/requirements.txt

RUN pip install --user -r requirements.txt

COPY --chown=appuser:appuser . /app

ENTRYPOINT [ "python", "main.py" ]
