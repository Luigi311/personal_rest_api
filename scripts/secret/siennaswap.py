import time
from scripts.selenium_helpers import setup_selenium, teardown_selenium
from bs4 import BeautifulSoup

from scripts.secret.functions import secretanalytics


def farming_earn_siennaswap():

    driver = setup_selenium()

    driver.get("https://app.sienna.network/swap/earn")

    time.sleep(6)

    # Put the page source into a variable and create a BS object from it
    soup_file = driver.page_source
    soup = BeautifulSoup(soup_file, features="html.parser")

    # Should point to the entire card that shows icon, name, pool size, pool rewards, and APR
    data = soup.find_all(
        "div",
        {"class": lambda L: L and L.startswith("SwapRewardCard__Card-aad4ay-0 dbDNse")},
    )

    output = {}

    for i in data:
        pool_size_index = i.text.find("Pool Size")
        pool_rewards_index = i.text.find("Pool Rewards")
        apr_index = i.text.find("APR")
        select_index = i.text.find("Select")
        key = i.text[:pool_size_index].replace("LP-", "")
        tvl = float(
            i.text[pool_size_index + len("Pool Size") : pool_rewards_index]
            .replace("$", "")
            .replace(",", "")
        )
        pool_rewards = float(
            i.text[pool_rewards_index + len("Pool Rewards") : apr_index].replace(
                "SIENNA / day", ""
            )
        )
        apr = (
            float(i.text[apr_index + len("APR") : select_index].replace("%", "")) / 100
        )

        output[key] = {"APR": apr, "TVL": tvl, "Pool_Rewards": pool_rewards}

    teardown_selenium(driver)

    return output


def protocol_siennaswap():
    output = secretanalytics("siennaswap")

    earn = farming_earn_siennaswap()

    for key in output:
        output[key]["Earn"] = {"APR": 0, "TVL": 0, "Pool_Rewards": 0}

        # If the key is in earn, add the earn values to the output
        if key in earn:
            output[key]["Earn"]["APR"] = earn[key]["APR"]
            output[key]["Earn"]["TVL"] = earn[key]["TVL"]
            output[key]["Earn"]["Pool_Rewards"] = earn[key]["Pool_Rewards"]

    return output


def farming_siennaswap():
    siennaswap = secretanalytics("siennaswap")

    output = {}
    for i in siennaswap:
        output[i] = siennaswap[i]["APR"]

    return output
