import time
from scripts.selenium_helpers import setup_selenium, teardown_selenium
from bs4 import BeautifulSoup

from scripts.secret.functions import secretanalytics


def farming_earn_secretswap():

    driver = setup_selenium()

    driver.get("https://app.secretswap.io/earn")

    # Wait for page to load
    time.sleep(6)

    # Confirm screen
    # pbutton = driver.find_elements_by_xpath("//*[@class='sc-htpNat joTfpG']")[0]
    # pbutton.click()

    # Hide inactive groups
    pbutton2 = driver.find_elements_by_xpath("//*[@class='slider round']")[0]
    pbutton2.click()

    # Put the page source into a variable and create a BS object from it
    soup_file = driver.page_source
    soup = BeautifulSoup(soup_file, features="html.parser")

    data = soup.find_all(
        "h3", {"class": lambda L: L and L.startswith("FTg73g7WoKiKIDYqtnj8ig==")}
    )

    counter = 0
    output = {}

    for i in data:
        # Get 0,3,6... and put it into a variable
        if counter % 3 == 0:
            key = i.text.replace(" ", "").upper()

        # Get 1,4,7... and put it into a variable
        elif counter % 3 == 1:
            apy = float(i.text.replace(" ", "").replace("%", "").replace(",", "")) / 100

        # Get 2,5,8... and put it into a variable
        elif counter % 3 == 2:
            tvl = float(i.text.replace(" ", "").replace("$", "").replace(",", ""))
            output[key] = {"APY": apy, "TVL": tvl}

        counter += 1

    teardown_selenium(driver)

    return output


def protocol_secretswap():
    output = secretanalytics("secretswap")

    earn = farming_earn_secretswap()

    for key in output:
        output[key]["Earn"] = {"APY": 0, "TVL": 0}

        # If the key is in earn, add the earn values to the output
        if key in earn:
            output[key]["Earn"]["APY"] = earn[key]["APY"]
            output[key]["Earn"]["TVL"] = earn[key]["TVL"]

    return output


def farming_secretswap():
    secretswap = secretanalytics("secretswap")

    output = {}
    for i in secretswap:
        output[i] = secretswap[i]["APR"]

    return output
