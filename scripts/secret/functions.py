import requests
from scripts.functions import parse_abbreviations


def secretanalytics(protocol: str):
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:92.0) Gecko/20100101 Firefox/92.0",
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-origin",
        "Pragma": "no-cache",
        "Cache-Control": "no-cache",
    }
    data = (
        '{"output":"page-content.children","outputs":{"id":"page-content","property":"children"},"inputs":[{"id":"url","property":"pathname","value":"/'
        + protocol
        + '"}],"changedPropIds":["url.pathname"]}'
    )
    response = requests.post(
        "https://secretanalytics.xyz/_dash-update-component", headers=headers, data=data
    ).json()
    output = {}
    exclusion_number = ["0", None, "NaN", 0]

    for i in response["response"]["page-content"]["children"]["props"]["children"][7][
        "props"
    ]["children"]["props"]["children"]["props"]["data"]:
        liquidity = parse_abbreviations(i["Liquidity"])
        fees = parse_abbreviations(i["Fees"])
        volume = parse_abbreviations(i["Volume"])

        if liquidity not in exclusion_number and fees not in exclusion_number:
            apr = fees * 365 / liquidity
        else:
            apr = 0

        output[i["Name"]] = {
            "Liquidity": liquidity,
            "Volume": volume,
            "Fees": fees,
            "APR": apr,
        }

    return output
