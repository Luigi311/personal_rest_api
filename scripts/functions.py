import os
from concurrent.futures import ThreadPoolExecutor

from scripts.lido.lido import staking_eth, staking_sol
from scripts.terra.staking import staking_luna


def staking_all():
    luna_apr = staking_luna()

    eth_apr = staking_eth()

    sol_apr = staking_sol()

    output = {"luna_apr": luna_apr, "eth_apr": eth_apr, "sol_apr": sol_apr}

    return output


def notify(message):
    print(message)


def parse_abbreviations(in_number):
    suffixes = ["", "K", "M", "B", "T"]
    output = None
    in_number = in_number.replace("$", "")
    abbreviation = in_number[-1].upper()

    if abbreviation in suffixes:
        just_number = float(in_number[:-1])
        output = just_number * (1000 ** suffixes.index(abbreviation))
    else:
        output = float(in_number)

    return output


def parse_dollar(in_number):
    in_number = in_number.replace("$", "")
    in_number = in_number.replace(",", "")
    output = float(in_number)
    return output


def future_thread_executor(args: list, workers: int = -1):
    futures_list = []
    results = []

    if workers == -1:
        workers = min(4, os.cpu_count()*1.25)

    with ThreadPoolExecutor(max_workers=workers) as executor:
        for arg in args:
            # * arg unpacks the list into actual arguments
            futures_list.append(executor.submit(*arg))

        for future in futures_list:
            try:
                result = future.result()
                results.append(result)
            except Exception as e:
                raise Exception(e)

    return results
