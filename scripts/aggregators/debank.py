import requests

def get_debank_wallet(addr):
    url = f"https://api.debank.com/user/addr?addr={addr}"
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()
    else:
        return None
