import time
from bs4 import BeautifulSoup

from scripts.selenium_helpers import setup_selenium, teardown_selenium

def uniswap_selenium(url, inputCurrency, outputCurrency, inputIndex, outputIndex):
    try:
        driver = setup_selenium(headless=True)

        driver.get(url+f"?inputCurrency={inputCurrency}&outputCurrency={outputCurrency}&chain=mainnet")
        time.sleep(6)
        
        try:
            if driver.find_element('xpath', '//button[text()="I understand"]'):
                driver.find_element('xpath', '//button[text()="I understand"]').click()
                time.sleep(1)
        except:
            pass

        driver.find_elements('xpath', '//input')[inputIndex].send_keys("1")
        time.sleep(6)
        # Put the page source into a variable and create a BS object from it
        soup_file = driver.page_source
        soup = BeautifulSoup(soup_file, features="html.parser")

        data = soup.find_all(
            "input", {"placeholder": "0"}
        )[outputIndex].get("value")

        if data == "":
            data = None
        else:
            data = float(data)

        # Close the browser
        teardown_selenium(driver)

        return data
    except Exception as e:
        print(e)
        raise Exception(f"Error in sushiswap_selenium")
