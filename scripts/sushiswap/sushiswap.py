import time
from bs4 import BeautifulSoup

from scripts.selenium_helpers import setup_selenium, teardown_selenium

def sushiswap_selenium(url, inputCurrency, outputCurrency, inputIndex):
    try:
        driver = setup_selenium(headless=True)
        outputIndex = 1
        if inputIndex == 1:
            outputIndex = 0

        time.sleep(1)
        driver.get(url+f"?inputCurrency={inputCurrency}&outputCurrency={outputCurrency}")
        time.sleep(6)

        driver.find_elements('xpath', '//input')[inputIndex].send_keys("1")
        time.sleep(6)
        # Put the page source into a variable and create a BS object from it
        soup_file = driver.page_source
        soup = BeautifulSoup(soup_file, features="html.parser")

        data = soup.find_all("input")[outputIndex].get("value")
        
        if data == "":
            data = None
        else:
            data = float(data)

        # Close the browser
        teardown_selenium(driver)

        return data
    except Exception as e:
        print(e)
        raise Exception(f"Error in sushiswap_selenium")
