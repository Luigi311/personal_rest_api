from scripts.functions import future_thread_executor

from scripts.ethereum.addresses import ethereum_addresses
from scripts.uniswap.uniswap import uniswap_selenium

def get_ethereum_uniswap():
    addresses = ethereum_addresses()

    uniswap_url = "https://app.uniswap.org/#/swap"

    args = [
        [uniswap_selenium, uniswap_url, addresses["sifu"], addresses["dai"], 2, 1],
        [uniswap_selenium, uniswap_url, addresses["sifu"], addresses["usdc"], 2, 1],
        [uniswap_selenium, uniswap_url, addresses["sifu"], addresses["usdt"], 2, 1],
        [uniswap_selenium, uniswap_url, addresses["dai"], addresses["sifu"], 3, 0],
        [uniswap_selenium, uniswap_url, addresses["usdc"], addresses["sifu"], 3, 0],
        [uniswap_selenium, uniswap_url, addresses["usdt"], addresses["sifu"], 3, 0],
    ]

    results = future_thread_executor(args)

    sifu_dai = results[0]
    sifu_usdc = results[1]
    sifu_usdt = results[2]
    dai_sifu = results[3]
    usdc_sifu = results[4]
    usdt_sifu = results[5]

    output = {
        "sifu_dai": sifu_dai,
        "sifu_usdc": sifu_usdc,
        "sifu_usdt": sifu_usdt,
        "dai_sifu": dai_sifu,
        "usdc_sifu": usdc_sifu,
        "usdt_sifu": usdt_sifu,
    }

    return output
