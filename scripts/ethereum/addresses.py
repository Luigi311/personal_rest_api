def ethereum_addresses():
    addresses = {
        "sifu": "0x29127fE04ffa4c32AcAC0fFe17280ABD74eAC313",
        "usdt": "0xdAC17F958D2ee523a2206206994597C13D831ec7",
        "usdc": "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48",
        "dai": "0x6B175474E89094C44Da98b954EedeAC495271d0F"
    }

    return addresses
