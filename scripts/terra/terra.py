import requests


def protocol_terra_output():
    output = {}

    # Get luna oracle price
    terra_oracle_luna_url = (
        "https://fcd.terra.dev/terra/oracle/v1beta1/denoms/uusd/exchange_rate"
    )
    terra_oracle_luna = float(
        requests.get(terra_oracle_luna_url).json()["exchange_rate"]
    )

    output["terra_oracle_luna"] = terra_oracle_luna

    return output
