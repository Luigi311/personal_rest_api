import requests, os
from dotenv import load_dotenv
from scripts.terra.stader import get_stader_delegations_terra

load_dotenv(override=True)

terra_public_node_url = os.getenv("TERRA_PUBLIC_NODE_URL")
sleep_duration = float(os.getenv("SLEEP_DURATION"))


def get_denominator_terra(token_address):
    denominator_url = (
        terra_public_node_url + "/wasm/contracts/" + token_address + "/store"
    )
    denominator_query_msg = '{"token_info": {}}'

    # Get "decimals" from response no matter how deep it is
    try:
        response = requests.get(
            url=denominator_url, params={"query_msg": denominator_query_msg}
        ).json()
        denominator_power = float(response["result"]["decimals"])
        denominator = 10 ** denominator_power
    except:
        raise Exception(
            "Could not get denominator for token "
            + token_address
            + ". URL: "
            + denominator_url
            + " Query_msg: "
            + denominator_query_msg
        )

    return denominator


def get_terraswap_tokens():
    output = {}

    tokens_url = "https://api.terraswap.io/tokens"
    tokens_response = requests.get(url=tokens_url).json()

    for token in tokens_response:
        output[token["contract_addr"]] = token["symbol"]

    return output


def get_terraswap_pairs():
    output = {}

    terra_tokens = get_terraswap_tokens()
    pairs_url = "https://api.terraswap.io/pairs"
    pairs_response = requests.get(url=pairs_url).json()

    for pair in pairs_response["pairs"]:
        token_pair_symbols = []
        token_pair_addr = []
        for token in pair["asset_infos"]:
            if "native_token" in token:
                token_pair_symbols.append(token["native_token"]["denom"])
                token_pair_addr.append(token["native_token"]["denom"])
            else:
                token_addrs = token["token"]["contract_addr"]
                if token_addrs in terra_tokens:
                    token_pair_symbols.append(terra_tokens[token_addrs])
                    token_pair_addr.append(token_addrs)
                else:
                    token_pair_symbols.append(token_addrs)
                    token_pair_addr.append(token_addrs)

        output[pair["contract_addr"]] = {
            "symbols": token_pair_symbols[0] + "-" + token_pair_symbols[1],
            "addresses": [token_pair_addr[0], token_pair_addr[1]],
        }

    return output


def get_native_balance_terra(wallet_address):
    output = {}

    url = terra_public_node_url + "/bank/balances/" + wallet_address
    response = requests.get(url=url).json()

    denominator = 10 ** 6

    for token in response["result"]:
        output[token["denom"]] = float(token["amount"]) / denominator

    return output


def get_token_wallet_balance_terra(token_address, wallet_address):
    if token_address.startswith("terra"):
        url = terra_public_node_url + "/wasm/contracts/" + token_address + "/store"
        query_msg = '{"balance":{"address":"' + wallet_address + '"}}'

        try:
            response = requests.get(url=url, params={"query_msg": query_msg}).json()
            denominator = get_denominator_terra(token_address)
            output = float(response["result"]["balance"]) / denominator
        except:
            print("Could not get token balance for " + token_address)
            return 0

        return output
    else:
        return 0


def get_token_supply_terra(token_address):
    url = terra_public_node_url + "/wasm/contracts/" + token_address + "/store"
    query_msg = '{"token_info": {}}'
    response = requests.get(url=url, params={"query_msg": query_msg}).json()

    output = float(response["result"]["total_supply"]) / get_denominator_terra(
        token_address
    )

    return output


def get_native_price_terra(token):
    output = 0
    url = "https://fcd.terra.dev/terra/oracle/v1beta1/denoms/uusd/exchange_rate"
    uusd_luna_price = float(requests.get(url=url).json()["exchange_rate"])

    if token == "uluna":
        output = uusd_luna_price

    elif token == "uusd":
        output = 1

    else:
        url = (
            "https://fcd.terra.dev/terra/oracle/v1beta1/denoms/"
            + token
            + "/exchange_rate"
        )
        token_luna_price = float(requests.get(url=url).json()["exchange_rate"])
        output = uusd_luna_price / token_luna_price

    return output


def get_token_price_terra(token_address):
    output = []

    terraswap_pairs = get_terraswap_pairs()
    terraswap_tokens = get_terraswap_tokens()

    amount = 1000  # 1000 due to simulation returning integer values instead of float
    contract_address = None
    symbol = token_address

    if token_address in terraswap_tokens:
        symbol = terraswap_tokens[token_address]

    if symbol == "aUST":
        url = "https://api.anchorprotocol.com/api/v1/market/ust"
        response = requests.get(url=url).json()
        output = [symbol, float(response["exchange_rate"])]
    elif symbol == "uLP":
        # LP Token
        token_total_value = 0

        token_url = terra_public_node_url + "/wasm/contracts/" + token_address
        token_response = requests.get(url=token_url).json()

        creator_addr = token_response["result"]["creator"]
        creator_url = (
            terra_public_node_url + "/wasm/contracts/" + creator_addr + "/store"
        )
        query_msg = '{"pool":{}}'
        lp_symbol = terraswap_pairs[creator_addr]["symbols"]

        pool = requests.get(url=creator_url, params={"query_msg": query_msg}).json()
        total_share = float(pool["result"]["total_share"])

        assets = pool["result"]["assets"]
        token_value = []
        for asset in assets:
            if "token" in asset["info"]:
                token_address = asset["info"]["token"]["contract_addr"]
                token_amount = float(asset["amount"])
                token_price = get_token_price_terra(token_address)[1]

                total_value = (token_amount / total_share) * token_price

                token_value.append(total_value)

            if "native_token" in asset["info"]:
                token_symbol = asset["info"]["native_token"]["denom"]
                token_amount = float(asset["amount"])
                token_price = get_native_price_terra(token_symbol)

                total_value = (token_amount / total_share) * token_price

                token_value.append(total_value)

        if len(token_value) == 2 and None not in token_value:
            token_total_value = sum(token_value)
            output = [lp_symbol, token_total_value]
        else:
            output = [lp_symbol, 0]
    else:
        for key, value in terraswap_pairs.items():
            if token_address in value["addresses"] and "uusd" in value["addresses"]:
                contract_address = key
                break

        if contract_address is not None:
            query_msg = (
                '{"simulation":{"offer_asset":{"amount":"'
                + str(amount)
                + '","info":{"token":{"contract_addr":"'
                + token_address
                + '"}}}}}'
            )
            response = requests.get(
                terra_public_node_url
                + "/wasm/contracts/"
                + contract_address
                + "/store",
                params={"query_msg": query_msg},
                timeout=sleep_duration,
            ).json()

            output = [symbol, float(response["result"]["return_amount"]) / amount]

        else:
            print("Error")
            output = 0

    return output


def get_token_marketcap_terra(token_address):
    price = get_token_price_terra(token_address)[1]
    supply = get_token_supply_terra(token_address)

    output = price * supply

    return output


def get_native_wallet_value_terra(wallet_address):
    output = {}

    balances = get_native_balance_terra(wallet_address)
    for key, amount in balances.items():
        output[key] = amount * get_native_price_terra(key)

    return output


def get_token_wallet_value_terra(token_address, wallet_address):
    output = []

    balance = get_token_wallet_balance_terra(token_address, wallet_address)
    token_price_array = get_token_price_terra(token_address)
    price = token_price_array[1]
    symbol = token_price_array[0]
    output = [symbol, balance * price]

    return output


def get_wallet_tokens(wallet_address):
    # Get all tokens in wallet
    terraswap_tokens = get_terraswap_tokens()

    for token in terraswap_tokens:
        token_address = token

        token_balance = get_token_wallet_balance_terra(token_address, wallet_address)
        if token_balance > 0:
            print(token_address, token_balance)


def get_wallet_value_terra(wallet_address, token_list):
    output = {}

    for token in token_list:
        if token == "native":
            value = get_native_wallet_value_terra(wallet_address)
            output = output | value
        elif token == "stader":
            amount = get_stader_delegations_terra(wallet_address)
            amount = sum(amount.values())

            luna_price = get_native_price_terra("uluna")

            output["stader"] = amount * luna_price
        else:
            value = get_token_wallet_value_terra(token, wallet_address)
            output[value[0]] = value[1]

    output["total"] = sum(output.values())

    return output


def get_smart_contract_deposit(contract_address, wallet_address):
    output = {}
    query_url = terra_public_node_url + "/wasm/contracts/" + contract_address + "/store"
    query_msg = '{"user_info": {"address": "' + wallet_address + '"}}'
    response = requests.get(url=query_url, params={"query_msg": query_msg}).json()

    return response
