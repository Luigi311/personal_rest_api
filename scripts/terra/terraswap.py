import requests
from scripts.terra.velocity import velocity_terraswap


def protocol_terraswap():
    headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Connection": "keep-alive",
        "DNT": "1",
        "Origin": "https://terraswap-graph.terra.dev",
    }
    data = '{"query": "{ pairs { pairAddress, latestLiquidityUST, token0{ tokenAddress, symbol }, token1{ tokenAddress, symbol } commissionAPR, volume24h{ volumeUST } } }"}'

    response = requests.post(
        "https://terraswap-graph.terra.dev/graphql", headers=headers, data=data
    ).json()
    output = {}
    exclusion_number = ["0", None, "NaN"]

    for i in response.get("data").get("pairs"):
        token_pair = f"{i.get('token0').get('symbol')}-{i.get('token1').get('symbol')}"

        if (
            i.get("token0").get("symbol") != None
            and i.get("token1").get("symbol") != None
            and i.get("latestLiquidityUST") not in exclusion_number
            and i.get("volume24h").get("volumeUST") not in exclusion_number
        ):
            volume = float(i.get("volume24h").get("volumeUST"))
            liquidity = float(i.get("latestLiquidityUST"))
            fees = volume * 0.003
            apr = fees * 365 / liquidity
            output[token_pair] = {
                "Liquidity": liquidity,
                "Volume": volume,
                "Fees": fees,
                "APR": apr,
            }

    return output


def farming_terraswap():
    terraswap = protocol_terraswap()
    output = {}
    for i in terraswap:
        output[i] = terraswap[i]["APR"]

    return output


def volume_terraswap():
    output = velocity_terraswap()
    return output
