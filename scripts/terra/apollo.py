import requests
from scripts.functions import parse_dollar
from scripts.terra.functions import (
    get_wallet_value_terra,
    get_token_wallet_balance_terra,
    get_token_supply_terra,
    get_token_marketcap_terra,
    get_token_price_terra,
    get_smart_contract_deposit,
    get_denominator_terra,
)


def protocol_apollo_output():
    output = {}
    apollo_warchest_wallet = "terra1hxrd8pnqytqpelape3aemprw3a023wryw7p0xn"
    token_apollo_uusd_LP_terraswap = "terra1n3gt4k3vth0uppk0urche6m3geu9eqcyujt88q"
    token_apollo = "terra100yeqvww74h4yaejj6h733thgcafdaukjtw397"
    token_pylon_mine = "terra1kcthelkax4j9x8d3ny6sdag0qmxxynl3qtcrpy"
    token_aust = "terra1hzh9vpxhsk8253se0vv5jj6etdvxu3nv8z07zu"
    apollo_warchest_tokens = [
        "native",
        "stader",
        token_pylon_mine,
        token_aust,
        token_apollo_uusd_LP_terraswap,
    ]

    warchest_test_value = 0
    astroport_lockdrop = "terra1627ldjvxatt54ydd3ns6xaxtd68a2vtyu7kakj"
    response = get_smart_contract_deposit(astroport_lockdrop, apollo_warchest_wallet)
    if response:
        try:
            test = float(response["result"]["lockup_infos"][0]["lp_units_locked"])
            if test > 0:
                test = test / (10 ** 6)
                warchest_test_value = (
                    test
                    * get_token_price_terra(
                        token_address=token_apollo_uusd_LP_terraswap
                    )[1]
                )
        except:
            pass

    # Get apollo TVL
    apollo_tvl = parse_dollar(
        requests.get("https://alpha-backend.vercel.app/api/landing-stats").json()["tvl"]
    )
    output["apollo_tvl"] = apollo_tvl

    # Get warchest balance
    warchest_balance = get_wallet_value_terra(
        apollo_warchest_wallet, apollo_warchest_tokens
    )

    warchest_balance["APOLLO-uusd"] = (
        warchest_balance["APOLLO-uusd"] + warchest_test_value
    )
    warchest_balance["total"] = warchest_balance["total"] + warchest_test_value
    output["warchest"] = warchest_balance

    # Get warchest infomation
    warchest_apollo_uusd_lp_balance = test
    # get_token_wallet_balance_terra(
    #    token_apollo_uusd_LP_terraswap, apollo_warchest_wallet
    # )

    apollo_uusd_lp_total_supply = get_token_supply_terra(token_apollo_uusd_LP_terraswap)
    warchest_apollo_uusd_lp_percentage = (
        warchest_apollo_uusd_lp_balance / apollo_uusd_lp_total_supply
    )

    apollo_token_price = get_token_price_terra(token_apollo)[1]
    apollo_token_supply = requests.get(
        "https://price-api-mainnet.apollo.farm/v1/apollo/supply"
    ).json()["supply"]
    # apollo_token_marketcap = get_token_marketcap_terra(apollo_token) # TODO: Currently pulls the entire token supply instead of just the circulating supply for none LP tokens
    apollo_token_marketcap = apollo_token_price * apollo_token_supply

    warchest_total_value = output["warchest"]["total"]
    warchest_over_apollo_mc_percentage = warchest_total_value / apollo_token_marketcap

    output["warchest_info"] = {
        "warchest_apollo_uusd_lp_balance": warchest_apollo_uusd_lp_balance,
        "warchest_apollo_uusd_lp_percentage": warchest_apollo_uusd_lp_percentage,
        "warchest_over_apollo_mc_percentage": warchest_over_apollo_mc_percentage,
    }

    output["apollo_token_info"] = {
        "apollo_token_marketcap": apollo_token_marketcap,
        "apollo_uusd_lp_total_supply": apollo_uusd_lp_total_supply,
        "apollo_token_price": apollo_token_price,
        "apollo_token_supply": apollo_token_supply,
    }

    return output
