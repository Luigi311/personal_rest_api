import requests
from scripts.terra.functions import get_token_wallet_balance_terra
from scripts.terra.staking import staking_luna
from scripts.lido.lido import staking_eth, staking_sol

delimiter = 1000000


def anchor_interest_caluclation():
    output = {}
    blocks_per_year = 4656810
    blocks_per_day = blocks_per_year / 365.25

    # Get anchor deposits
    anchor_deposits_url = "https://api.anchorprotocol.com/api/v1/deposit"
    total_ust_deposits = (
        float(requests.get(anchor_deposits_url).json()["total_ust_deposits"])
        / delimiter
    )

    anchor_overseer_url = "https://lcd.terra.dev/wasm/contracts/terra1tmnqgvg567ypvsvk6rwsga3srp7e3lg6u0elp8/store?query_msg="

    deposit_rate = float(
        requests.get(anchor_overseer_url + "{%22epoch_state%22:{}}").json()["result"][
            "deposit_rate"
        ]
    )
    total_daily_interest_required = deposit_rate * blocks_per_day * total_ust_deposits

    anchor_overseer = requests.get(anchor_overseer_url + "{%22config%22:{}}").json()[
        "result"
    ]
    threshold_interest_rate = (
        float(anchor_overseer["threshold_deposit_rate"]) * blocks_per_year
    )
    target_interest_rate = (
        float(anchor_overseer["target_deposit_rate"]) * blocks_per_year
    )

    # Get Anchor Colaterals
    anchor_collateral_url = "https://api.anchorprotocol.com/api/v1/collaterals"

    bluna_collateral = 0
    bluna_price = 0
    beth_collateral = 0
    beth_price = 0
    bsol_collateral = 0
    bsol_price = 0

    collateral_res = requests.get(anchor_collateral_url).json()

    borrow_collateral_total_value = float(collateral_res["total_value"]) / delimiter
    for i in collateral_res["collaterals"]:
        if i["symbol"] == "bLUNA":
            bluna_collateral = float(i["collateral"]) / delimiter
            bluna_price = float(i["price"])
        elif i["symbol"] == "bETH":
            beth_collateral = float(i["collateral"]) / delimiter
            beth_price = float(i["price"])
        elif i["symbol"] == "bSOL":
            bsol_collateral = float(i["collateral"]) / delimiter
            bsol_price = float(i["price"])

    # Calculate ust value of collaterals
    bluna_ust_collateral = bluna_collateral * bluna_price
    beth_ust_collateral = beth_collateral * beth_price
    bsol_ust_collateral = bsol_collateral * bsol_price

    # Get staking rewards
    bluna_staking_rewards = staking_luna()
    beth_staking_rewards = staking_eth()
    bsol_staking_rewards = staking_sol()

    # Calculate daily interest for each collateral
    bluna_daily_interest = bluna_ust_collateral * bluna_staking_rewards / 365
    beth_daily_interest = beth_ust_collateral * beth_staking_rewards / 365
    bsol_daily_interest = bsol_ust_collateral * bsol_staking_rewards / 365

    # Get Anchor Borrows
    anchor_borrow_url = "https://api.anchorprotocol.com/api/v1/borrow"
    total_borrowed = (
        float(requests.get(anchor_borrow_url).json()["total_borrowed"]) / delimiter
    )

    # Calculate interest rate
    anchor_interest_url = "https://lcd.terra.dev/wasm/contracts/terra1kq8zzq5hufas9t0kjsjc62t2kucfnx8txf547n/store?query_msg={%22config%22:{}}"

    interest_config = requests.get(anchor_interest_url).json()["result"]
    interest_multiplier = float(interest_config["interest_multiplier"])
    base_rate = float(interest_config["base_rate"])
    interest_rate = (
        total_borrowed / total_ust_deposits * interest_multiplier * blocks_per_year
    ) + (base_rate * blocks_per_year)

    # Calculate borrow interest earned
    borrow_interest_earned = total_borrowed * interest_rate
    borrow_daily_interest = borrow_interest_earned / 365

    # Calculate daily percent of interest earned
    # Use threshold to calculate percent as total_daily_interest_required is calculating the threshold value required
    beth_daily_percent = (
        beth_daily_interest / total_daily_interest_required * threshold_interest_rate
    )
    bluna_daily_percent = (
        bluna_daily_interest / total_daily_interest_required * threshold_interest_rate
    )
    bsol_daily_percent = (
        bsol_daily_interest / total_daily_interest_required * threshold_interest_rate
    )
    borrow_daily_percent = (
        borrow_daily_interest / total_daily_interest_required * threshold_interest_rate
    )

    # Calculate total numbers
    total_daily_percent = (
        beth_daily_percent
        + bluna_daily_percent
        + bsol_daily_percent
        + borrow_daily_percent
    )
    total_daily_interest = (
        beth_daily_interest
        + bluna_daily_interest
        + bsol_daily_interest
        + borrow_daily_interest
    )

    # Output
    output["asset_bluna_collateral"] = bluna_collateral
    output["asset_bluna_price"] = bluna_price
    output["asset_bluna_ust_collateral"] = bluna_ust_collateral
    output["asset_bluna_staking_rewards"] = bluna_staking_rewards
    output["asset_beth_collateral"] = beth_collateral
    output["asset_beth_price"] = beth_price
    output["asset_beth_ust_collateral"] = beth_ust_collateral
    output["asset_beth_staking_rewards"] = beth_staking_rewards
    output["asset_bsol_collateral"] = bsol_collateral
    output["asset_bsol_price"] = bsol_price
    output["asset_bsol_ust_collateral"] = bsol_ust_collateral
    output["asset_bsol_staking_rewards"] = bsol_staking_rewards

    output["daily_interest_bluna"] = bluna_daily_interest
    output["daily_interest_beth"] = beth_daily_interest
    output["daily_interest_bsol"] = bsol_daily_interest
    output["daily_interest_borrow"] = borrow_daily_interest

    output["daily_percent_beth"] = beth_daily_percent
    output["daily_percent_bluna"] = bluna_daily_percent
    output["daily_percent_bsol"] = bsol_daily_percent
    output["daily_percent_borrow"] = borrow_daily_percent

    output["borrow_total_borrow"] = total_borrowed
    output["borrow_borrow_interest_rate"] = interest_rate
    output["borrow_collateral_total_value"] = borrow_collateral_total_value

    output["earn_total_deposit"] = total_ust_deposits
    output["earn_threshold_interest_rate"] = threshold_interest_rate
    output["earn_target_interest_rate"] = target_interest_rate

    output["total_daily_percent"] = total_daily_percent
    output["total_daily_interest"] = total_daily_interest
    output["total_daily_interest_required"] = total_daily_interest_required

    output["anchor_oracle_luna"] = bluna_price
    output["anchor_oracle_eth"] = beth_price
    output["anchor_oracle_sol"] = bsol_price

    return output


def protocol_anchor():
    output = {}

    anchor_reserve_url = "https://lcd.terra.dev/bank/balances/terra1tmnqgvg567ypvsvk6rwsga3srp7e3lg6u0elp8"
    anchor_anc_distribution_url = (
        "https://api.anchorprotocol.com/api/v2/distribution-apy"
    )

    # Get reserve balance
    anchor_reserve_balance = (
        float(requests.get(anchor_reserve_url).json()["result"][0]["amount"])
        / delimiter
    )

    # Get anc distribution
    anchor_distribution = requests.get(anchor_anc_distribution_url).json()
    anchor_distribution_apy = float(anchor_distribution["distribution_apy"])

    # Get distributor balance
    anchor_distributor_balance = get_token_wallet_balance_terra(
        "terra14z56l0fp2lsf86zy3hty2z47ezkhnthtr9yq76",
        "terra1mxf7d5updqxfgvchd7lv6575ehhm8qfdttuqzz",
    )

    output["earn_reserve"] = anchor_reserve_balance
    output["anc_distributor_balance"] = anchor_distributor_balance
    output["borrow_distribution_interst_rate"] = anchor_distribution_apy

    # Get interest calculation
    interest = anchor_interest_caluclation()
    output.update(interest)

    # Get farming information
    farming = farming_anchor()
    output.update(farming)

    output["tvl"] = (
        output["borrow_collateral_total_value"] + output["earn_total_deposit"]
    )

    return output


def farming_anchor():
    output = {}

    # Get ANC statistics
    gov_staking_url = "https://api.anchorprotocol.com/api/v2/gov-reward"
    gov_staking_interest = float(requests.get(gov_staking_url).json()["current_apy"])
    output["gov_staking_interest"] = gov_staking_interest

    gov_lp_url = "https://api.anchorprotocol.com/api/v2/ust-lp-reward"
    gov_lp_interest = float(requests.get(gov_lp_url).json()["apy"])
    output["gov_lp_interest"] = gov_lp_interest

    return output
