import requests
from dotenv import load_dotenv
import os

load_dotenv(override=True)

terra_public_node_url = os.getenv("TERRA_PUBLIC_NODE_URL")
sleep_duration = float(os.getenv("SLEEP_DURATION"))


def get_stader_delegations_terra(wallet_address):
    output = {}

    contract_address = "terra1t9ree3ftvgr70fvm6y67zsqxjms8jju8kwcsdu"
    query_msg = '{"user": {"user_addr":"' + wallet_address + '"}}'

    response = requests.get(
        terra_public_node_url + "/wasm/contracts/" + contract_address + "/store",
        params={"query_msg": query_msg},
        timeout=sleep_duration,
    ).json()

    for pool in response["result"]["info"]:
        pool_id = pool["pool_id"]
        deposit_amount = float(pool["deposit"]["staked"])

        output[pool_id] = deposit_amount / 10 ** 6

    return output
