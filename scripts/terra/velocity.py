import requests, json


def sort_list(in_list):
    output = sorted(in_list, key=lambda x: x[0])
    return output


def build_output(data, amount_type, output):
    for i in data:
        if i[0] not in output:
            output[i[0]] = {}

        if i[1] not in output[i[0]]:
            output[i[0]][i[1]] = {}

        if amount_type not in output[i[0]][i[1]]:
            output[i[0]][i[1]][amount_type] = 0

        output[i[0]][i[1]][amount_type] = output[i[0]][i[1]][amount_type] + i[2]

    return output


def velocity_terraswap():
    output = {}
    commission = []
    volume = []
    url = "https://api.flipsidecrypto.com/api/v2/queries/9d0cba5f-b956-4ec9-8bd1-9e88e2f6f9b1/data/latest"
    response = requests.get(url).json()

    for i in response:
        events = json.loads(i["EVENT_ATTRIBUTES"])
        index_count = 0

        # Temp arrays to hold data when multiple swaps are involved to ensure the correct order
        commissiontemp = []
        volumetemp = []
        temp2 = []
        for key, value in events.items():
            if value == "swap":
                try:
                    # if more than 1 action
                    under_index = key.index("_action")
                    number = key[:under_index]
                    # Only a single swap event per contract
                    if "ask_asset" in events:
                        commission.append(
                            [
                                events[str(number) + "_contract_address"],
                                events["ask_asset"],
                                events["commission_amount"],
                            ]
                        )
                        volume.append(
                            [
                                events[str(number) + "_contract_address"],
                                events["offer_asset"],
                                events["offer_amount"],
                            ]
                        )
                    else:
                        # Add to temp arrays to ensure correct order of contract numbers
                        commissiontemp.append(
                            [
                                events[str(index_count) + "_ask_asset"],
                                events[str(index_count) + "_commission_amount"],
                            ]
                        )
                        volumetemp.append(
                            [
                                events[str(index_count) + "_offer_asset"],
                                events[str(index_count) + "_offer_amount"],
                            ]
                        )
                        temp2.append(
                            [int(number), events[str(number) + "_contract_address"]]
                        )

                        index_count += 1
                # If only one action
                except ValueError:
                    if "ask_asset" in events:
                        commission.append(
                            [
                                events["contract_address"],
                                events["ask_asset"],
                                events["commission_amount"],
                            ]
                        )
                        volume.append(
                            [
                                events["contract_address"],
                                events["offer_asset"],
                                events["offer_amount"],
                            ]
                        )

        # Sort temp arrays to ensure correct order of contract numbers
        if len(temp2) > 0:
            temp3 = sort_list(temp2)
            for j, k in enumerate(temp3):
                commission.append(
                    [temp3[j][1], commissiontemp[j][0], commissiontemp[j][1]]
                )
                volume.append([temp3[j][1], volumetemp[j][0], volumetemp[j][1]])

    build_output(commission, "commission", output)
    build_output(volume, "volume", output)

    return output
