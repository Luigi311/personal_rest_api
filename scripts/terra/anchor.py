import requests, json
from scripts.terra.functions import (
    get_token_wallet_balance_terra,
    get_denominator_terra,
)
from scripts.terra.staking import staking_luna, staking_atom
from scripts.lido.lido import staking_eth, staking_sol
from scripts.selenium_helpers import get_by_class

delimiter = 1000000
anchor_overseer_addr = "terra1tmnqgvg567ypvsvk6rwsga3srp7e3lg6u0elp8"
anchor_interest_model_addr = "terra1kq8zzq5hufas9t0kjsjc62t2kucfnx8txf547n"


def anchor_interest_caluclation():
    output = {}
    blocks_per_year = 4656810
    blocks_per_day = blocks_per_year / 365.25
    daily_anc_buyback = 0

    mantle_url = "https://mantle.terra.dev"

    # Get anchor deposits
    anchor_deposits_url = "https://api.anchorprotocol.com/api/v1/deposit"
    total_ust_deposits = (
        float(requests.get(anchor_deposits_url).json()["total_ust_deposits"])
        / delimiter
    )
    anchor_overseer_url = (
        f"https://lcd.terra.dev/wasm/contracts/{anchor_overseer_addr}/store?query_msg="
    )
    anchor_overseer_epoch_state = requests.get(
        anchor_overseer_url + "{%22epoch_state%22:{}}"
    ).json()["result"]
    anchor_overseer_config = requests.get(
        anchor_overseer_url + "{%22config%22:{}}"
    ).json()["result"]

    deposit_rate = float(anchor_overseer_epoch_state["deposit_rate"])
    epoch_period = float(anchor_overseer_config["epoch_period"])
    compound_times = blocks_per_year / epoch_period
    per_compound = deposit_rate * epoch_period
    anchor_apy = pow((per_compound + 1), compound_times) - 1

    total_daily_interest_required = (anchor_apy * total_ust_deposits) / 365

    anc_buyback_percentage = float(anchor_overseer_config["anc_purchase_factor"])

    # Get Anchor Colaterals
    anchor_collateral_url = "https://api.anchorprotocol.com/api/v2/collaterals"

    bluna_collateral = 0
    bluna_price = 0
    beth_collateral = 0
    beth_price = 0
    bsol_collateral = 0
    bsol_price = 0
    batom_collateral = 0
    batom_price = 0

    collateral_res = requests.get(anchor_collateral_url).json()

    borrow_collateral_total_value = float(collateral_res["total_value"]) / delimiter
    for i in collateral_res["collaterals"]:
        if i["symbol"] == "bLuna":
            bluna_collateral = float(i["collateral"]) / get_denominator_terra(
                i["token"]
            )
            bluna_price = float(i["price"])
        elif i["symbol"] == "bETH":
            beth_collateral = float(i["collateral"]) / get_denominator_terra(i["token"])
            beth_price = float(i["price"])
        elif i["symbol"] == "bSOL":
            bsol_collateral = float(i["collateral"]) / get_denominator_terra(i["token"])
            bsol_price = float(i["price"])
        elif i["symbol"] == "bATOM":
            batom_collateral = float(i["collateral"]) / get_denominator_terra(
                i["token"]
            )
            batom_price = float(i["price"])

    # Calculate ust value of collaterals
    bluna_ust_collateral = bluna_collateral * bluna_price
    beth_ust_collateral = beth_collateral * beth_price
    bsol_ust_collateral = bsol_collateral * bsol_price
    batom_ust_collateral = batom_collateral * batom_price

    # Get staking rewards
    bluna_staking_rewards = staking_luna()
    beth_staking_rewards = staking_eth()
    bsol_staking_rewards = staking_sol()
    batom_staking_rewards = staking_atom()

    # Calculate daily interest for each collateral
    bluna_daily_interest = bluna_ust_collateral * bluna_staking_rewards / 365
    beth_daily_interest = beth_ust_collateral * beth_staking_rewards / 365
    bsol_daily_interest = bsol_ust_collateral * bsol_staking_rewards / 365
    batom_daily_interest = batom_ust_collateral * batom_staking_rewards / 365

    # Remove anc buyback from daily interest
    daily_anc_buyback += bluna_daily_interest * anc_buyback_percentage
    daily_anc_buyback += beth_daily_interest * anc_buyback_percentage
    daily_anc_buyback += bsol_daily_interest * anc_buyback_percentage
    daily_anc_buyback += batom_daily_interest * anc_buyback_percentage

    bluna_daily_interest = bluna_daily_interest * (1 - anc_buyback_percentage)
    beth_daily_interest = beth_daily_interest * (1 - anc_buyback_percentage)
    bsol_daily_interest = bsol_daily_interest * (1 - anc_buyback_percentage)
    batom_daily_interest = batom_daily_interest * (1 - anc_buyback_percentage)

    # Get Anchor Borrows
    anchor_borrow_url = "https://api.anchorprotocol.com/api/v1/borrow"
    total_borrowed = (
        float(requests.get(anchor_borrow_url).json()["total_borrowed"]) / delimiter
    )

    # Calculate interest rate
    anchor_interest_url = (
        "https://lcd.terra.dev/wasm/contracts/"
        + anchor_interest_model_addr
        + "/store?query_msg={%22config%22:{}}"
    )

    interest_config = requests.get(anchor_interest_url).json()["result"]
    interest_multiplier = float(interest_config["interest_multiplier"])
    base_rate = float(interest_config["base_rate"])
    interest_rate = (
        total_borrowed / total_ust_deposits * interest_multiplier * blocks_per_year
    ) + (base_rate * blocks_per_year)

    # Calculate borrow interest earned
    borrow_interest_earned = total_borrowed * interest_rate
    borrow_daily_interest = borrow_interest_earned / 365

    # Calculate daily percent of interest earned
    beth_daily_percent_sub = ((beth_daily_interest * 365) / total_ust_deposits)
    beth_daily_percent = (
        pow((1 + (beth_daily_percent_sub / blocks_per_year)), blocks_per_year) - 1
    )

    bluna_daily_percent_sub = ((bluna_daily_interest * 365) / total_ust_deposits)
    bluna_daily_percent = (
        pow((1 + (bluna_daily_percent_sub / blocks_per_year)), blocks_per_year) - 1
    )

    bsol_daily_percent_sub = ((bsol_daily_interest * 365) / total_ust_deposits)
    bsol_daily_percent = (
        pow((1 + (bsol_daily_percent_sub / blocks_per_year)), blocks_per_year) - 1
    )

    batom_daily_percent_sub = ((batom_daily_interest * 365) / total_ust_deposits)
    batom_daily_percent = (
        pow((1 + (batom_daily_percent_sub / blocks_per_year)), blocks_per_year) - 1
    )

    borrow_daily_percent_sub = (
        (borrow_daily_interest * 365) / total_ust_deposits
    )
    borrow_daily_percent = (
        pow((1 + (borrow_daily_percent_sub / blocks_per_year)), blocks_per_year) - 1
    )

    # Calculate total numbers
    total_daily_percent = (
        beth_daily_percent
        + bluna_daily_percent
        + bsol_daily_percent
        + batom_daily_percent
        + borrow_daily_percent
    )
    total_daily_interest = (
        beth_daily_interest
        + bluna_daily_interest
        + bsol_daily_interest
        + batom_daily_interest
        + borrow_daily_interest
    )

    # Return aust exchange rate
    #  curl 'https://mantle.terra.dev/' -H 'Accept-Encoding: gzip, deflate, br' -H 'Content-Type: application/json'  --data-binary '{"query":"{  moneyMarketEpochState: WasmContractsContractAddressStore(    ContractAddress: \"terra1sepfj7s0aeg5967uxnfk4thzlerrsktkpelm5s\"    QueryMsg: \"{\\\"epoch_state\\\":{}}\"  ) {    Result    Height  } }"}' --compressed
    data = {
        "query": '{  moneyMarketEpochState: WasmContractsContractAddressStore(    ContractAddress: "terra1sepfj7s0aeg5967uxnfk4thzlerrsktkpelm5s"    QueryMsg: "{\\"epoch_state\\":{}}"  ) {    Result    Height  } }'
    }
    headers = {
        "Accept-Encoding": "gzip, deflate, br",
        "Content-Type": "application/json",
    }
    res = requests.post(mantle_url, headers=headers, data=json.dumps(data)).json()
    aust_exchange_rate = float(
        json.loads(res["data"]["moneyMarketEpochState"]["Result"])["exchange_rate"]
    )

    # Output
    output["asset_bluna_collateral"] = bluna_collateral
    output["asset_bluna_price"] = bluna_price
    output["asset_bluna_ust_collateral"] = bluna_ust_collateral
    output["asset_bluna_staking_rewards"] = bluna_staking_rewards
    output["asset_beth_collateral"] = beth_collateral
    output["asset_beth_price"] = beth_price
    output["asset_beth_ust_collateral"] = beth_ust_collateral
    output["asset_beth_staking_rewards"] = beth_staking_rewards
    output["asset_bsol_collateral"] = bsol_collateral
    output["asset_bsol_price"] = bsol_price
    output["asset_bsol_ust_collateral"] = bsol_ust_collateral
    output["asset_bsol_staking_rewards"] = bsol_staking_rewards
    output["asset_batom_collateral"] = batom_collateral
    output["asset_batom_price"] = batom_price
    output["asset_batom_ust_collateral"] = batom_ust_collateral
    output["asset_batom_staking_rewards"] = batom_staking_rewards

    output["daily_interest_bluna"] = bluna_daily_interest
    output["daily_interest_beth"] = beth_daily_interest
    output["daily_interest_bsol"] = bsol_daily_interest
    output["daily_interest_batom"] = batom_daily_interest
    output["daily_interest_borrow"] = borrow_daily_interest
    output["daily_anc_buyback"] = daily_anc_buyback

    output["daily_percent_beth"] = beth_daily_percent
    output["daily_percent_bluna"] = bluna_daily_percent
    output["daily_percent_bsol"] = bsol_daily_percent
    output["daily_percent_batom"] = batom_daily_percent
    output["daily_percent_borrow"] = borrow_daily_percent

    output["borrow_total_borrow"] = total_borrowed
    output["borrow_borrow_interest_rate"] = interest_rate
    output["borrow_collateral_total_value"] = borrow_collateral_total_value

    output["earn_total_deposit"] = total_ust_deposits

    output["total_daily_percent"] = total_daily_percent
    output["total_daily_interest"] = total_daily_interest
    output["total_daily_interest_required"] = total_daily_interest_required

    output["anchor_oracle_luna"] = bluna_price
    output["anchor_oracle_eth"] = beth_price
    output["anchor_oracle_sol"] = bsol_price
    output["anchor_oracle_atom"] = batom_price

    output["anchor_earn_apy"] = anchor_apy

    output["anchor_oracle_aust"] = aust_exchange_rate

    return output


def protocol_anchor():
    output = {}

    anchor_reserve_url = "https://lcd.terra.dev/bank/balances/" + anchor_overseer_addr
    anchor_anc_distribution_url = (
        "https://api.anchorprotocol.com/api/v2/distribution-apy"
    )

    # Get reserve balance
    anchor_reserve_balance = (
        float(requests.get(anchor_reserve_url).json()["result"][0]["amount"])
        / delimiter
    )

    # Get anc distribution
    anchor_distribution = requests.get(anchor_anc_distribution_url).json()
    anchor_distribution_apy = float(anchor_distribution["distribution_apy"])

    # Get distributor balance
    anchor_distributor_balance = get_token_wallet_balance_terra(
        "terra14z56l0fp2lsf86zy3hty2z47ezkhnthtr9yq76",
        "terra1mxf7d5updqxfgvchd7lv6575ehhm8qfdttuqzz",
    )

    output["earn_reserve"] = anchor_reserve_balance
    output["anc_distributor_balance"] = anchor_distributor_balance
    output["borrow_distribution_interst_rate"] = anchor_distribution_apy

    # Get interest calculation
    interest = anchor_interest_caluclation()
    output.update(interest)

    # Get farming information
    farming = farming_anchor()
    output.update(farming)

    output["tvl"] = (
        output["borrow_collateral_total_value"] + output["earn_total_deposit"]
    )

    return output


def farming_anchor():
    output = {}

    # Get ANC statistics
    gov_staking_url = "https://api.anchorprotocol.com/api/v2/gov-reward"
    gov_staking_interest = float(requests.get(gov_staking_url).json()["current_apy"])
    output["gov_staking_interest"] = gov_staking_interest

    # Get gov_lp_interest
    gov_url = "https://app.anchorprotocol.com/gov"
    gov_lp_interest_class_name = "sc-djUGQo lnbMHd"

    # curl 'https://api.astroport.fi/graphql' -X POST --data-raw '{"query":"\n  query {\n    pools {\n      pool_address\n      token_symbol\n      trading_fee\n      pool_liquidity\n      _24hr_volume\n      trading_fees {\n        apy\n        apr\n        day\n      }\n      astro_rewards {\n        apy\n        apr\n        day\n      }\n      protocol_rewards {\n        apy\n        apr\n        day\n      }\n      total_rewards {\n        apy\n        apr\n        day\n      }\n    }\n  }\n"}'

    json_data = {
        "query": "\n  query {\n    pools {\n      pool_address\n total_rewards {\n        apy\n        apr\n        day\n      }\n    }\n  }\n",
    }

    astroport_apy = requests.post(
        "https://api.astroport.fi/graphql", json=json_data
    ).json()

    pools = astroport_apy["data"]["pools"]
    for pool in pools:
        if pool["pool_address"] == "terra1qr2k6yjjd5p2kaewqvg93ag74k6gyjr7re37fs":
            gov_lp_interest = float(pool["total_rewards"]["apr"])
            break

    output["gov_lp_interest"] = gov_lp_interest

    return output
