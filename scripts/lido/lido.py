from scripts.selenium_helpers import setup_selenium, teardown_selenium
from bs4 import BeautifulSoup
import cloudscraper

lido_eth_url = "https://stake.lido.fi/api/apr"
lido_luna_url = "https://lido.fi/terra"
lido_solana_url = "https://lido.fi/solana"


def lido_selenium(url):
    driver = setup_selenium()

    driver.get(url)

    # Put the page source into a variable and create a BS object from it
    soup_file = driver.page_source
    soup = BeautifulSoup(soup_file, features="html.parser")

    data = soup.find_all(
        "p", {"class": lambda L: L and L.startswith("InfoFieldStyle__Data-")}
    )[1].get_text()

    data = float(data.replace("%", "")) / 100

    # Close the browser
    teardown_selenium(driver)

    return data


def staking_eth():
    scraper = cloudscraper.create_scraper()

    eth = scraper.get(lido_eth_url).json()
    eth_apr = float(eth["data"]["steth"]) / 100

    return eth_apr


def staking_sol():
    return lido_selenium(lido_solana_url)

def staking_luna_lido():
    return lido_selenium(lido_luna_url)
