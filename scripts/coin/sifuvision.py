from scripts.aggregators.debank import get_debank_wallet

def get_sifuvision():
    total_supply = 1_000_000
    circulating_supply = 733086.4742
    wallet_balance = get_debank_wallet("0x31d3243cfb54b34fc9c73e1cb1137124bd6b13e1")

    debank_protocol_balance = wallet_balance["protocol_usd_value"]
    debank_balance = wallet_balance["usd_value"]


    debank_value = debank_balance - debank_protocol_balance

    backing_price = debank_value / circulating_supply

    output = {
        "debank_protocol_balance": debank_protocol_balance,
        "debank_balance": debank_balance,
        "debank_value": debank_value,
        "debank_backing_price": backing_price
    }

    return output