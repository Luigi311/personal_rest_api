from scripts.functions import future_thread_executor
from scripts.avalanche.addresses import avalanche_addresses
from scripts.sushiswap.sushiswap import sushiswap_selenium

def get_avalanche_sushiswap():
    addresses = avalanche_addresses()

    sushiswap_url = "https://avalanche.sushi.com/swap"

    args = [
        [sushiswap_selenium, sushiswap_url, addresses["sifu"], addresses["dai"], 0],
        [sushiswap_selenium, sushiswap_url, addresses["sifu"], addresses["usdc"], 0],
        [sushiswap_selenium, sushiswap_url, addresses["sifu"], addresses["usdt"], 0],
        [sushiswap_selenium, sushiswap_url, addresses["dai"], addresses["sifu"], 1],
        [sushiswap_selenium, sushiswap_url, addresses["usdc"], addresses["sifu"], 1],
        [sushiswap_selenium, sushiswap_url, addresses["usdt"], addresses["sifu"], 1],
    ]

    results = future_thread_executor(args)

    sifu_dai = results[0]
    sifu_usdc = results[1]
    sifu_usdt = results[2]
    dai_sifu = results[3]
    usdc_sifu = results[4]
    usdt_sifu = results[5]

    output = {
        "sifu_dai": sifu_dai,
        "sifu_usdc": sifu_usdc,
        "sifu_usdt": sifu_usdt,
        "dai_sifu": dai_sifu,
        "usdc_sifu": usdc_sifu,
        "usdt_sifu": usdt_sifu,
    }

    return output
