def avalanche_addresses():
    addresses = {
        "sifu": "0x237917E8a998b37759c8EE2fAa529D60c66c2927",
        "usdt": "0xc7198437980c041c805A1EDcbA50c1Ce5db95118",
        "usdc": "0xA7D7079b0FEaD91F3e65f86E8915Cb59c1a4C664",
        "dai": "0xd586E7F844cEa2F87f50152665BCbc2C279D8d70"
    }

    return addresses