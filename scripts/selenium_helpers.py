from selenium import webdriver
from dotenv import load_dotenv
from bs4 import BeautifulSoup
import os, time

load_dotenv(override=True)

def setup_selenium(metamask=False, headless=False):
    # Set options for the browser
    options = webdriver.ChromeOptions()
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--disable-gpu")
    options.add_argument("--no-sandbox")
    if headless:
        options.add_argument("--headless")

    if metamask:
        print("Setting up metamask")
        options.add_extension('./MetaMask_10.12.4_0.crx')

    driver = webdriver.Remote(
            command_executor=os.getenv("SELENIUM_ADDRESS_PORT"),
            options=options,
        )

    if metamask:
        time.sleep(15)
        print("Metamask is ready")
        # Switch to second tab
        driver.switch_to.window(driver.window_handles[1])
        # Close tab
        driver.close()
        # Switch to first tab
        driver.switch_to.window(driver.window_handles[0])

        driver.get(
        'chrome-extension://nkbihfbeogaeaoehlefnkodbefgpgknn/home.html#initialize/create-password')  # Go straight to creating Metamaks wallet

        time.sleep(5)
        inputs = driver.find_elements_by_xpath('//input')
        inputs[0].send_keys('Test1234')
        inputs[1].send_keys('Test1234')
        driver.find_element_by_css_selector('.first-time-flow__checkbox').click()
        driver.find_element_by_xpath('//button[text()="Create"]').click()
        time.sleep(5)
        driver.find_element_by_xpath('//button[text()="Next"]').click()
        driver.find_element_by_xpath('//button[text()="Remind me later"]').click()

    return driver


def teardown_selenium(driver):
    driver.quit()


def get_by_class(url, class_name, timer: float = 6):
     # Get gov_lp_interest
    driver = setup_selenium()
    driver.get(url)
    time.sleep(timer)
 
    soup_file = driver.page_source
    soup = BeautifulSoup(soup_file, features="html.parser")
    data = soup.find_all(class_=class_name)
    teardown_selenium(driver)
    
    return data













import io, logging, re, requests, subprocess, zipfile, sys

logger = logging.getLogger("personal_rest_api")

CHROME_DRIVER_BASE_URL = "https://chromedriver.storage.googleapis.com/"
CHROME_DRIVER_DOWNLOAD_PATH = "{version}/chromedriver_{arch}.zip"
CHROME_DRIVER_LATEST_RELEASE = "LATEST_RELEASE"
CHROME_ZIP_TYPES = {
    "linux": "linux64",
    "linux2": "linux64",
    "darwin": "mac64",
    "win32": "win32",
    "win64": "win32",
}
version_pattern = re.compile(
    "(?P<version>(?P<major>\\d+)\\.(?P<minor>\\d+)\\."
    "(?P<build>\\d+)\\.(?P<patch>\\d+))"
)


def get_chrome_driver_url(version, arch):
    return CHROME_DRIVER_BASE_URL + CHROME_DRIVER_DOWNLOAD_PATH.format(
        version=version, arch=CHROME_ZIP_TYPES.get(arch)
    )


def get_chrome_driver_major_version_from_executable(local_executable_path):
    # Note; --version works on windows as well.
    # check_output fails if running from a thread without a console on win10.
    # To protect against this use explicit pipes for STDIN/STDERR.
    # See: https://github.com/pyinstaller/pyinstaller/issues/3392
    with open(os.devnull, "wb") as devnull:
        version = subprocess.check_output(
            [local_executable_path, "--version"], stderr=devnull, stdin=devnull
        )
        version_match = version_pattern.search(version.decode())
        if not version_match:
            return None
        return version_match.groupdict()["major"]


def get_latest_chrome_driver_version():
    """Returns the version of the latest stable chromedriver release."""
    latest_url = CHROME_DRIVER_BASE_URL + CHROME_DRIVER_LATEST_RELEASE
    latest_request = requests.get(latest_url)

    if latest_request.status_code != 200:
        raise RuntimeError(
            "Error finding the latest chromedriver at {}, status = {}".format(
                latest_url, latest_request.status_code
            )
        )
    return latest_request.text


def get_stable_chrome_driver(download_directory=os.getcwd()):
    chromedriver_name = "chromedriver"
    if sys.platform in ["win32", "win64"]:
        chromedriver_name += ".exe"

    local_executable_path = os.path.join(download_directory, chromedriver_name)

    latest_chrome_driver_version = get_latest_chrome_driver_version()
    version_match = version_pattern.match(latest_chrome_driver_version)
    latest_major_version = None
    if not version_match:
        logger.error(
            "Cannot parse latest chrome driver string: {}".format(
                latest_chrome_driver_version
            )
        )
    else:
        latest_major_version = version_match.groupdict()["major"]
    if os.path.exists(local_executable_path):
        major_version = get_chrome_driver_major_version_from_executable(
            local_executable_path
        )
        if major_version == latest_major_version or not latest_major_version:
            # Use the existing chrome driver, as it's already the latest
            # version or the latest version cannot be determined at the moment.
            return local_executable_path
        logger.info("Removing old version {} of Chromedriver".format(major_version))
        os.remove(local_executable_path)

    if not latest_chrome_driver_version:
        logger.critical(
            "No local chrome driver found and cannot parse the latest chrome "
            "driver on the internet. Please double check your internet "
            "connection, then ask for assistance on the github project."
        )
        return None
    logger.info(
        "Downloading version {} of Chromedriver".format(latest_chrome_driver_version)
    )
    zip_file_url = get_chrome_driver_url(latest_chrome_driver_version, sys.platform)
    request = requests.get(zip_file_url)

    if request.status_code != 200:
        raise RuntimeError(
            "Error finding chromedriver at {}, status = {}".format(
                zip_file_url, request.status_code
            )
        )

    zip_file = zipfile.ZipFile(io.BytesIO(request.content))
    zip_file.extractall(path=download_directory)
    os.chmod(local_executable_path, 0o755)
    return local_executable_path