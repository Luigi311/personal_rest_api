import time
from scripts.selenium_helpers import setup_selenium, teardown_selenium
from bs4 import BeautifulSoup

def get_pstake_apr():
    driver = setup_selenium(metamask=True)
    driver.get("https://app.pstake.finance/")

    time.sleep(5)
    driver.find_element_by_xpath('//input[@type="checkbox" and @id="accept"]').click()
    driver.find_element_by_xpath('//button[text()="Proceed"]').click()

    time.sleep(5)
    driver.find_element_by_xpath('//div[text()="Connect to your MetaMask Wallet"]').click()
    time.sleep(5)
    driver.switch_to.window(driver.window_handles[1])
    driver.find_element_by_xpath('//button[text()="Next"]').click()
    driver.find_element_by_xpath('//button[text()="Connect"]').click()

    time.sleep(40)
    driver.switch_to.window(driver.window_handles[0])

    soup_file = driver.page_source
    soup = BeautifulSoup(soup_file, features="html.parser")

    data = soup.find_all("h6")

    for i in data:
        if i.text.find("%") != -1:
            apr = i.text
            break

    apr = apr.replace("%", "")
    
    teardown_selenium(driver)

    return float(apr)/100